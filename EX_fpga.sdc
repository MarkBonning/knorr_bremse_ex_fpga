## Generated SDC file "PVU.sdc"

## Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, the Altera Quartus Prime License Agreement,
## the Altera MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Altera and sold by Altera or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 15.1.0 Build 185 10/21/2015 SJ Lite Edition"

## DATE    "Fri Jan 22 08:34:34 2016"

##
## DEVICE  "10M08SAU169I7G"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -period 50.000 -waveform { 0.000 25.000 } [get_ports CLOCK_20MHZ]
create_clock -period 50.000 -name clock_20Mhz_virt

create_clock -period 250.000 -waveform { 0.000 125.000 } [get_ports SPI2_CLK]
create_clock -period 250.000 -name spi2_clk_virt

create_clock -period 250.000 -waveform { 0.000 125.000 } [get_ports LW_FPGA_SPI0_CLK]
create_clock -period 250.000 -name lw_fpga_spi0_clk_virt

#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks



#**************************************************************
# constrain JTAG
#**************************************************************
#create_clock -period 5MHz {altera_reserved_tck}
#set_clock_groups -asynchronous -group {altera_reserved_tck}
#set_input_delay -clock {altera_reserved_tck} 20 [get_ports altera_reserved_tdi]
#set_input_delay -clock {altera_reserved_tck} 20 [get_ports altera_reserved_tms]
#set_output_delay -clock {altera_reserved_tck} 20 [get_ports altera_reserved_tdo]



#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************

#**************************************************************
# Set Output Delay
#**************************************************************

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************
#for async signals


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

