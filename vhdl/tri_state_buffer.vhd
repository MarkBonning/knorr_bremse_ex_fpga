-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  tri_state_buffer.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/11/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Tri state output buffer 
-- Quartus ALT_OUTBUF_TRI is not compatible with MAX10 parts
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity tri_state_buffer is
    port (
	 
		-- in
		in_i	: in std_logic;
		out_o  : out std_logic;

		enable_i	: in std_logic
		
    );
end entity tri_state_buffer;

architecture imp_tri_state_buffer of tri_state_buffer is

	

begin


-- logic

	out_o <= in_i when enable_i = '1' else 'Z';
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_tri_state_buffer;

