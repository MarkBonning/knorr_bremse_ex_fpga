-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  dac_MAX5705.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/11/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls an SPI master to drive the MAX5705 DAC
-- for test FPGAs only
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
USE ieee.std_logic_arith.all;


entity dac_max5705 is
    port 	(

      	clock20mhz_i        :  in std_logic;
      	reset_ni            :  in std_logic;
        
     	enable_o			: out std_logic;
      	cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		addr_o				: out std_logic_vector(7 downto 0);
		tx_data_o			: out std_logic_vector(23 downto 0);
		busy_i				: in std_logic;
		rx_data_i			: in std_logic_vector(15 downto 0);
      	aux_o				: out std_logic

        );
end;

architecture RTL of dac_max5705 is


type states is ( REF, REF_WAIT, DEFAULT, DEFAULT_WAIT, CODE_CHANGE, CODE, CODE_WAIT);
signal state   	: states;

signal rx_data_s	: std_logic_vector(15 downto 0);

begin

	-- drive constant pins
	cpol_o <= '0';
	cpha_o <= '0';
	addr_o <= (others => '0');
	cont_o <= '0';

	-- map signals to ports
	


    -- counter to set the watchdog refresh signal
    s25fl128s_flash_read : 	process ( reset_ni, clock20mhz_i ) is

	variable count : integer range 0 to 255:= 0;
    
    
    begin
        if ( reset_ni = '0' ) then
            enable_o <= '0';
            count := 0;
			rx_data_o <= (others => '0');
			rx_data_s <= (others => '0');
            state <= REF;

        elsif Rising_Edge ( clock20mhz_i ) then
        

-- state machine to run the transfer to the SPI module

----------------------------------------------------
-- Send REF value
----------------------------------------------------

					case state is
						when REF =>
							enable_o <= '1';
							tx_data_o <= x"250000";
							if busy_i = '0' then
								state <= REF;
							else
								state <= REF_WAIT;
							end if;
						when REF_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								state <= DEFAULT;
							else
								state <= REF_WAIT;
							end if;

----------------------------------------------------
-- Send DEFAULT value
----------------------------------------------------

					case state is
						when DEFAULT =>
							enable_o <= '1';
							tx_data_o <= x"250000";
							if busy_i = '0' then
								state <= DEFAULT;
							else
								state <= DEFAULT_WAIT;
							end if;
						when DEFAULT_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								state <= CODE_CHANGE;
							else
								state <= DEFAULT_WAIT;
							end if;

----------------------------------------------------
-- Send CODE value
----------------------------------------------------

					case state is
						when CODE_CHANGE =>
							if (rx_data_s = rx_data_i) then
								state <= CODE_CHANGE;
							else
								rx_data_s <= rx_data_i;
								state <= CODE;
							end if;
						
						when CODE =>
							enable_o <= '1';
							tx_data_o <= x"80" & rx_data_s & "0000";
							if busy_i = '0' then
								state <= CODE;
							else
								state <= CODE_WAIT;
							end if;
						when CODE_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								state <= CODE;
							else
								state <= CODE_WAIT;
							end if;

		        		when others =>
		        			count := 0;
							state <= REF;
					end case;		          
					
        end if;
    end process;


end architecture RTL;