-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  digital_input.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  13/05/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Digital input filter
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity digital_input is
	port 	(
	
		clock10k_i					: in std_logic;							-- slow clock 10kHz, saves on large counter numbers
		reset_ni					: in std_logic;		
		
		DIG_INPUTX_i				: in std_logic;							-- external digital input signal
		pulse_enable_i					: in std_logic;							-- logic 1 to enable the digital input (form register)
		
		Filtered_Dig_InputX_o		: out std_logic;						-- filtered digital output (to register)

		DIG_INPUTX_PULSE_ENABLE_o	: out std_logic							-- external pulse enable output signal
		
		);
end; 

architecture RTL of digital_input is

	-- signals
	signal Filtered_Dig_InputX_s		: std_logic;
	signal DIG_INPUTX_PULSE_ENABLE_s	: std_logic;

	signal	pulse_timer_s				: std_logic;
	
	
begin
		
--map registers to ports
Filtered_Dig_InputX_o <= Filtered_Dig_InputX_s;
DIG_INPUTX_PULSE_ENABLE_o <= DIG_INPUTX_PULSE_ENABLE_s;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to filter the digital input
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_Filtered_Dig_InputX : process (reset_ni, clock10k_i)

	variable count_30ms	      : integer range 0 to 300 := 0;
	constant count_30ms_limit : integer range 0 to 300 := 300;				-- 30ms (100us x 300)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count_30ms := 0;
			Filtered_Dig_InputX_s <= DIG_INPUTX_i;

		-- clock data in on rising edge of slow clock
		elsif rising_edge(clock10k_i) then	 

			-- if input = output then clear the count
			if Filtered_Dig_InputX_s = DIG_INPUTX_i then 
				count_30ms := 0;
			else
				count_30ms := count_30ms + 1;
				if count_30ms = count_30ms_limit then
					Filtered_Dig_InputX_s <= DIG_INPUTX_i;
				end if;
			end if;
			
	 	end if;
	end process;
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to create pulse timer
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_pulse_timer : process (reset_ni, clock10k_i)

	variable count_500ms      : integer range 0 to 5000 := 0;
	constant count_500ms_limit : integer range 0 to 5000 := 5000;				-- 500ms (100us x 5000)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count_500ms := 0;
			pulse_timer_s	<= '0';

		-- clock data in on rising edge of slow clock
		elsif rising_edge(clock10k_i) then	 

			
			if pulse_timer_s = '1' and count_500ms < count_500ms_limit then
				count_500ms := count_500ms + 1;
				
			elsif Filtered_Dig_InputX_s = '0' and DIG_INPUTX_i = '1' then
				count_500ms := 0;
				pulse_timer_s	<= '1';
				
			else
				pulse_timer_s	<= Filtered_Dig_InputX_s;
			end if;

	 	end if;
	end process;	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to control DIGITAL INPUT X PULSE ENABLE
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_Digital_InputX_Pulse_Enable : process (reset_ni, clock10k_i)

	variable count_62ms	      : integer range 0 to 620 := 0;
	constant count_62ms_limit : integer range 0 to 620 := 620;					-- 620ms (100us x 620)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count_62ms := 0;
			DIG_INPUTX_PULSE_ENABLE_s <= '0';

		-- clock data in on rising edge of slow clock
		elsif rising_edge(clock10k_i) then	 

			if pulse_timer_s = '0' then
				count_62ms := 0;
				DIG_INPUTX_PULSE_ENABLE_s <= '1' and pulse_enable_i;
			else
				if count_62ms < count_62ms_limit then
					DIG_INPUTX_PULSE_ENABLE_s <= '1' and pulse_enable_i;
					count_62ms := count_62ms + 1;
				else
					DIG_INPUTX_PULSE_ENABLE_s <= '0';
				end if;
			end if;
			
	 	end if;
	end process;
	
end architecture RTL;