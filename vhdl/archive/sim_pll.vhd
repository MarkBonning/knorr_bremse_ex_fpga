-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  PVU Test FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  sim_pll.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Quartus PLL does not simulate. This file replaces the pll for simulating the design
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sim_pll is
	port 	(
	
		clock_i			: in std_logic;		
		clock_o			: out std_logic;
		reset_o			: out std_logic
		
		);
end; 

architecture RTL of sim_pll is

	-- signals

	signal clock_s						: std_logic;
	
	
begin

	-- map registers to ports
	clock_o <= clock_s;
		
	div2 : process (clock_i)

	variable count_s	      : integer range 0 to 65535 := 0;
	
	begin

		-- clock data in on rising edge of clock
		if rising_edge(clock_i) then	 

			clock_s <= not clock_s;
			
			if( count_s < 2 ) then
				reset_o <= '0';
				count_s := count_s + 1;
			else
				reset_o <= '1';
			end if;
			
		
			
	 	end if;
	end process;

	
	

	
end architecture RTL;