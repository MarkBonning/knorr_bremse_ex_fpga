-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_interface.vhd
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Interface to the internal adc hardcore
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. 
--             Changes to channel control       
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.pvuConstants.all;

entity adc_interface is
	port 	(
		clock10mhz_adc     	:  in std_logic; 
		reset  			    	:  in std_logic;

		reset_sink_reset_n  	: out std_logic;
		response_valid      	:  in std_logic;
		response_channel    	:  in std_logic_vector( 4 downto 0);
		response_data       	:  in std_logic_vector(11 downto 0);
		sequencer_address   	: out std_logic;
		sequencer_write     	: out std_logic;
		sequencer_writedata 	: out std_logic_vector(31 downto 0);
		bcp1						: out std_logic_vector(11 downto 0);
		bcp2						: out std_logic_vector(11 downto 0)
		);
end;

architecture RTL of adc_interface is
	signal adc_register_channel : std_logic_vector( 4 downto 0);
	signal counter              : unsigned( 3 downto 0);
	signal adc_register         : unsigned(11 downto 0);
	signal vlcp_1               : unsigned(11 downto 0);
	signal asp1_1               : unsigned(11 downto 0);
	signal asp2_1               : unsigned(11 downto 0);
	signal vlcp_int             : unsigned(11 downto 0);
	signal asp1_int             : unsigned(11 downto 0);
	signal asp2_int             : unsigned(11 downto 0);
	
	type state_cm_wr is ( ready, adc_start, adc_run );

	signal set_sync_sm     :	state_cm_wr;

	attribute keep                    		: boolean;

	attribute keep of response_valid       : signal is true;
	attribute keep of response_channel     : signal is true;
	attribute keep of response_data        : signal is true;
	attribute keep of sequencer_write      : signal is true;
	attribute keep of sequencer_writedata  : signal is true;

begin

	reset_sink_reset_n <= '1';
	sequencer_address  <= '0';

	-- receive data from adc
	receive_data :process (reset, clock10mhz_adc) is
	begin
		if reset = RESETACTIVELEVEL then
			adc_register_channel <=  ( others => '0' ) ;
			adc_register <= ( others => '0' ) ;
			counter <=  ( others => '0' );
		elsif falling_edge ( clock10mhz_adc ) then
			if counter = "1011" then
				counter <= "0000";
			elsif response_valid = '1' then -- valid for one clock cycle
				counter <=  counter + 1;
				adc_register_channel <= response_channel;
				adc_register <=  unsigned(response_data);
			end if;
		end if;
	end process;

	compare_vlcp : process (clock10mhz_adc, reset) is
	begin
		if reset = RESETACTIVELEVEL then
			vlcp_compare <= '0';
		elsif rising_edge ( clock10mhz_adc ) then
			if  response_channel = "01000" then
				vlcp_compare <= '1';
			else
				vlcp_compare <= '0';
			end if;
		end if;
	end process;

	-- latch data to be correct register depending on the 4-bit address written
	address_decode_data : process (clock10mhz_adc, reset) is
	begin
		if reset = RESETACTIVELEVEL then
			bcp1 			<= "000000000000";
			bcp2 			<= "000000000000";
			vlcp_int 	<= "000000000000";
			asp1_int 	<= "000000000000";
			asp2_int 	<= "000000000000";
			temperature	<= "000000000000";
		elsif rising_edge ( clock10mhz_adc ) then
			if adc_register_channel = "00001" then	   --1
				vlcp_int <= adc_register;
			elsif adc_register_channel = "00010" then --2
				asp1_int <= adc_register;
			elsif adc_register_channel = "00011" then --3
				asp2_int <= adc_register;
			elsif adc_register_channel = "00111" then --4
				bcp1 <= std_logic_vector(adc_register);
			elsif adc_register_channel = "01000" then --5
				bcp2 <= std_logic_vector(adc_register);
			elsif adc_register_channel = "10001" then --6
				temperature <= std_logic_vector(adc_register);
			end if;
		end if;
	end process;

	vlcp <= std_logic_vector(vlcp_int);
	asp1 <= std_logic_vector(asp1_int);
	asp2 <= std_logic_vector(asp2_int);
	
	
-- sets the adc start signal	
	process (reset, clock10mhz_adc) is
		variable count : integer range 0 to 10:= 0;
	begin
		if reset = RESETACTIVELEVEL then
			set_sync_sm <= READY;
			sequencer_write 		<= '0';
			sequencer_writedata 	<=  (others => '0') ;
		elsif Rising_Edge ( clock10mhz_adc ) then
			case set_sync_sm is
				when READY =>
					count := count + 1;
					if count = 9 then
						set_sync_sm      		<= adc_start;
					else
						set_sync_sm      		<= READY;
					end if;
				when adc_start =>
					sequencer_write 			<= '1';
					sequencer_writedata(0) 	<= '1';
					set_sync_sm      			<= adc_run;
				when adc_run =>
					sequencer_write 			<= '0';
					sequencer_writedata 		<= (others => '0') ;
					set_sync_sm  				<= adc_run;
			end case;
		end if;
	end process;


end architecture RTL;