-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pwm_input.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  14/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- This module counts the number of 10MHz clock cycles between tacho pulses
-- The count is 24 bits. Should the count overflow into the 25th bit, then a
-- minimum speed flag is set.
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity tacho_input is
	port 	(
	
		clock10m_i			: in std_logic;		
		reset_ni			: in std_logic;		
		
		Tacho_Input_i		: in std_logic;									-- tacho pulse input
		Pulse_Count_o		: out std_logic_vector(23 downto 0);			-- current pulse count output
		Min_Speed_Detect_o 	: out std_logic;								-- flag to indicate minimum speed
		tacho_pwm_write_o 	: out std_logic;								-- write flag to register mux
		
		);
end; 

architecture RTL of tacho_input is

-- signals
type states is ( PULSE, CLEAR);
signal state_s   	: states;

-- local count. Has extra bit to allow for overrun detection
signal Pulse_Count_s : unsigned(24 downto 0);

-- rising edge detect signals
signal s0_s							: std_logic;								-- edge detect signal 0
signal s1_s							: std_logic;								-- edge detect signal 1
signal rising_detect_s				: std_logic;								-- single clock pulse on rising edge


begin

	
-- single clock pulse on rising edge of Tacho_Input_i
rising_detect_s <= not s1_s and s0_s;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Rising edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, clock10mhz_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock10mhz_i) then
			s0_s <= Tacho_Input_i;
			s1_s <= s0_s;
		end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Tacho count logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

	tacho_count : process (reset_ni, clock10m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			Pulse_Count_s <= (others => '0');
			Min_Speed_Detect_o <= '0';
			state_s <= PULSE;

		-- clock data in on rising edge of clock
		elsif rising_edge(clock10m_i) then	 
		
			case state_s is

				-- main state		
				when PULSE =>		

					-- if top bit set, then overrun occured. Set min speed flag and clear count
					if Pulse_Count_s(24) = '1' then
						Min_Speed_Detect_o = '1';
						Pulse_Count_o <= (others => '0');
						tacho_pwm_write_o <= '1';
						state_s <= CLEAR;

					-- on rising edge load counts. These are valid as the overrun is caught above
					elsif rising_detect_s = '1' then
						tacho_pwm_write_o <= '1';
						Min_Speed_Detect_o = '0';
						Pulse_Count_o <= Pulse_Count_s(23 downto 0);
						state_s <= CLEAR;
					-- else increment the count
					else
						Pulse_Count_s <= Pulse_Count_s + 1;
						state_s <= PULSE;
					end if;

				-- 
				when CLEAR =>		
					tacho_pwm_write_o <= '0';
					Pulse_Count_o <= (others => '0');

				-- something went wrong, go back to reset
				when others =>
					state_s <= PULSE;

			end case;
			
	 	end if;
	end process;
	
end architecture RTL;