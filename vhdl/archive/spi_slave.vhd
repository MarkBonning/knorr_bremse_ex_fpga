-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  PVU Test FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  spi_slave.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generic spi slave interface. To be used on all FPGAs within the project
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. Changed to generic
--             design to connect to a register mux       
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_slave is
	port 	(
		spi_clk_i      				: in std_logic;  								-- spi clk from master
		spi_cs_ni        				: in std_logic;  								-- active low slave select
		spi_mosi_i         			: in std_logic;  								-- master out, slave in
		spi_miso_o         			: out std_logic;  							-- master in, slave out
		address_o						: out std_logic_vector(4 downto 0); 	-- register address to mux
		data_i  							: in std_logic_vector(15 downto 0); 	-- data from mux
		data_o							: out std_logic_vector(15 downto 0);	-- data to mux
		write_o							: out std_logic;								-- write to mux
		load_o							: out std_logic								-- load from mux
		);
end; 

architecture RTL of spi_slave is

	signal rx_data_s		: std_logic_vector(23 downto 0);						-- spi recieve buffer (including address byte)
	signal transmit_s		: std_logic;
	signal tx_data_s  	: std_logic_vector(15 downto 0);  					-- spi transmit buffer (data only)
	signal address_s		: std_logic_vector(4 downto 0);						-- register addres
	signal load_s			: std_logic;
	signal write_s			: std_logic;
	signal read_write_s	: std_logic;												-- 0 = read request, 1 = write request

begin
		
	--map registers to ports
	data_o 		<= rx_data_s(15 downto 0);
	load_o		<= load_s;
	write_o		<= write_s;
	address_o	<= address_s;
	

	-- receive data from spi bus
	receive_data : process (spi_cs_ni, spi_clk_i)
	variable cntr	      : integer range 0 to 30:= 0;
	
	begin
		-- clear registers when cs is high
		if(spi_cs_ni = '1') then
			rx_data_s			<= (others => '0');
--			address_s			<= (others => '0');
			transmit_s			<= '0';
			load_s				<= '0';
			write_s 				<= '0';
			
			cntr				:= 0;	
			
		-- clock data in on rising edge of spi clock
		elsif rising_edge(spi_clk_i) then	 

			rx_data_s <= rx_data_s(22 downto 0) & spi_mosi_i;  

			-- load the register address
			if ( cntr =  5) then
				address_s <= rx_data_s(3 downto 0) & spi_mosi_i; -- address
				read_write_s <= rx_data_s(4);						 -- read or write
			-- flag we need the data present on data_i
			elsif( cntr = 6) then
				if (read_write_s = '0') then							 -- 0 = read, 1 = write
					load_s <= '1';											
				end if;
			-- flag we need to start transmitting the data
			elsif(cntr = 7) then
				transmit_s <= '1';
				load_s <= '0';			

				
			-- flag that the data is present on data_o (until spi cs goes high)
			elsif(cntr = 23) then
				if (read_write_s = '1') then							 -- 0 = read, 1 = write
					write_s <= '1';
				end if;
			end if;
			
			-- increment count if less than 25. Stopping at 25 protects the range of cntr if an spi transfer goes on too long (more bytes)
			if(cntr < 25) then
				cntr := cntr + 1;
			end if;
			
	
	 	end if;
	end process;

	
	-- transmit data to spi bus
	transmit_data : process (spi_cs_ni, spi_clk_i, data_i)
	begin
		-- clear registers when cs is high
		if(spi_cs_ni = '1') then
			tx_data_s <= (others => '0');
			spi_miso_o <= 'Z';
			
		--clock data out on falling edge of spi clock
		elsif falling_edge(spi_clk_i) then

			-- load data when available. This will be after the register address has been decoded, just before
			-- data is required to be clocked out.
			
			if load_s = '1' then
				tx_data_s <= data_i;
--				tx_data_s <= x"1234";
	
			-- transmit flag states we are ready to clock out the data
			elsif transmit_s = '1' then	
				tx_data_s(15 downto 0) <= tx_data_s(14 downto 0) & tx_data_s(15);  --shift through tx data
				spi_miso_o <= tx_data_s(15);  
			else
				spi_miso_o <= '0';	
			end if;
		end if;
	end process;
	
	
end architecture RTL;