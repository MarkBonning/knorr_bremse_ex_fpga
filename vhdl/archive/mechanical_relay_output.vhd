-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  mechanical_relay_output.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Mechanical relay driver
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mechanical_relay_output is
	port 	(
	
		clock20m_i					: in std_logic;		
		reset_ni						: in std_logic;		
		
		relayRegister_i			: in std_logic;
		readRelayRegister_i		: in std_logic;
		ncInput_i					: in std_logic;
		noInput_i					: in std_logic;
		
		relayDrive_o				: out std_logic;
		readRelayDive_o			: out std_logic;
		relay_5_nc				: out std_logic;
		relay_5_no				: out std_logic
		
		);
end; 

architecture RTL of mechanical_relay_output is

	-- signals
	signal readRelayState_s		: std_logic;
	
	
begin
		
	--map registers to ports
	relayDrive_o <= relayRegister_i;
	relay_5_nc <= ncInput_i;
	relay_5_no <= noInput_i;
	
	
	-- 
	read : process (reset_ni, clock20m_i)

	variable count	      : integer range 0 to 1000000 := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count := 0;
			readRelayDive_o <= '0';
			readRelayState_s <= '0';

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			-- if register is the same as current state, zero the count 
			if( readRelayState_s = relayRegister_i) then
				count := 0;
				-- reed relay state = reed relay register value
				readRelayDive_o <= readRelayRegister_i;
			-- else, increment the count
			else 
				count := count + 1;
				-- reed realy state = 1
				readRelayDive_o <= '1';
			end if;
			
			-- if the count gets to 1 million = 50ms then take the new input value
			if( count = 1000000) then
				readRelayState_s <= relayRegister_i;
			else
				readRelayState_s <= readRelayState_s;				
			end if;
			
	 	end if;
	end process;
	
end architecture RTL;