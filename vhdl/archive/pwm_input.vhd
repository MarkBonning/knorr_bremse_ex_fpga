-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pwm_input.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/12/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Measures PWM high pulse width and period in clock cycles
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity pwm_input is
	port 	(
	
		clock20m_i			: in std_logic;		
		reset_ni			: in std_logic;		
		
		PWMInput_i			: in std_logic;
		
		clock_divider_i		: in std_logic_vector(2 downto 0);
		
		periodCount_o		: out std_logic_vector(15 downto 0);
		highCount_o			: out std_logic_vector(15 downto 0);
		pwm_write_o			: out std_logic
		
		);
end; 

architecture RTL of pwm_input is

	-- signals
	type states is ( RESET, START, SIGNAL_HIGH, SIGNAL_LOW);
	signal state_s   	: states;

	signal pwm_clock_s	: std_logic;
	signal pwm_clock_count_s : std_logic_vector(4 downto 0);
	
begin
		
	--map registers to ports

	proc_pwm_clock : process (reset_ni, clock20m_i)
	
	begin
		if rising_edge(clock20m_i) then	 

			pwm_clock_count_s <= std_logic_vector(unsigned(pwm_clock_count_s + 1));
			
			pwm_clock_s <= pwm_clock_count_s(unsigned(clock_divider_i));

	 	end if;
	end process;
	
	
	
	
	-- 
	pwm_read : process (reset_ni, pwm_clock_s)

	variable period_s	      : integer range 0 to 65535 := 0;
	variable high_s	      : integer range 0 to 65535 := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			period_s := 0;
			high_s := 0;
			periodCount_o <= (others => '0');
			highCount_o <= (others => '0');
			state_s <= RESET;

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

		
		case state_s is
			-- only used after a reset
			when RESET =>
				-- if signal goes low, we can get ready to start. This state stops the first read being erroneous			
				if(PWMInput_i = '0') then
					period_s := 0;
					high_s := 0;
					pwm_write_o <= '0';					
					state_s <= START;
					
				else
					state_s <= RESET;
				end if;
			-- wait for signal to go high		
			when START =>		
				pwm_write_o <= '0';
				if(PWMInput_i = '1') then
					period_s := 0;
					high_s := 0;
					state_s <= SIGNAL_HIGH;
				else
					state_s <= START;
				end if;

			-- signal is high, count highs and period
			when SIGNAL_HIGH =>		
				period_s := period_s +1;
				high_s := high_s + 1;

				if(PWMInput_i = '1') then
					state_s <= SIGNAL_HIGH;
				else
					state_s <= SIGNAL_LOW;
				end if;

			-- signal is low, count period
			when SIGNAL_LOW =>		

				period_s := period_s +1;
				-- stay here while low
				if(PWMInput_i = '0') then
					state_s <= SIGNAL_LOW;
				else
					-- finished, so copy counts to outputs
					periodCount_o <= std_logic_vector(to_unsigned(period_s, periodCount_o'length));
					highCount_o <= std_logic_vector(to_unsigned(high_s, highCount_o'length));
					pwm_write_o <= '1';
					state_s <= START;
				end if;

			-- something went wrong, go back to reset
			when others =>
				state_s <= RESET;

		end case;
			
	 	end if;
	end process;
	
	
		
	
end architecture RTL;