-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pvu_register_mux.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  10/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register mux. Multiplexer for the spi interface to the registers
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pvu_register_mux is
	port 	(
	
		clock20m_i						: in std_logic;		
		reset_ni							: in std_logic;		
		
		address_i						: in std_logic_vector(5 downto 0); 		-- register address from spi module
		spi_data_i						: in std_logic_vector(15 downto 0); 	-- data from spi module
		spi_data_o						: out std_logic_vector(15 downto 0);	-- data to spi module
		spi_write_i						: in std_logic;								-- write data to spi module
		spi_load_i						: in std_logic;								-- load spi data from spi module
		
		-- Input registers
		digital_in_i						: in std_logic_vector(6 downto 0); 	-- 
		register_1_i					: in std_logic_vector(15 downto 0); 	-- 
		register_2_i					: in std_logic_vector(15 downto 0); 	-- 
		register_3_i					: in std_logic_vector(15 downto 0); 	-- 
		register_4_i					: in std_logic_vector(15 downto 0) 	-- 
		
		
		);
end; 

architecture RTL of pvu_register_mux is

	-- signals
	signal spi_data_in_s				: std_logic_vector(15 downto 0);
	signal spi_data_out_s			: std_logic_vector(15 downto 0);
	signal address_read_s			: std_logic_vector(5 downto 0);
	
	signal register_0_s				: std_logic_vector(15 downto 0);
	signal register_1_s				: std_logic_vector(15 downto 0);
	signal register_2_s				: std_logic_vector(15 downto 0);
	signal register_3_s				: std_logic_vector(15 downto 0);
	signal register_4_s				: std_logic_vector(15 downto 0);
	
	
begin
		
	--map registers to ports
	spi_data_o <= spi_data_out_s;

	
	
	-- transmit data to spi module from registers
	mux_read : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			address_read_s <= (others => '0');
			spi_data_out_s	<= (others => '0');
			register_0_s <= (others => '0');
			register_1_s <= (others => '0');
			register_2_s <= (others => '0');
			register_3_s <= (others => '0');
			register_4_s <= (others => '0');
			
			
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 


			address_read_s <= address_i;
		
			
			-- handle write registers
			if ( spi_load_i = '1' ) then
			
				case address_read_s is

						
					when "000000" => spi_data_out_s <= register_0_s;
					when "000001" => spi_data_out_s <= register_1_s;
					when "000010" => spi_data_out_s <= register_2_S;
					when "000011" => spi_data_out_s <= register_3_S;
					when "000100" => spi_data_out_s <= register_4_S;
					
					when others => null;
				end case;
				
				register_0_s <= register_0_s;
				register_1_s <= register_1_s;
				register_2_s <= register_2_s;
				register_3_s <= register_3_s;
				register_4_s <= register_4_s;
				
			else
			
				register_0_s <= "000000000" & digital_in_i;
				register_1_s <= register_1_i;
				register_2_s <= register_2_i;
				register_3_s <= register_3_i;
				register_4_s <= register_4_i;			
			
			end if;

			
			
	 	end if;
	end process;
	
	
end architecture RTL;