-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  dac_MAX5705.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/11/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls an SPI master to drive the MAX5705 DAC
-- for test FPGAs only
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
USE ieee.std_logic_arith.all;


entity dac_max5705 is
    port 	(

      	clock20mhz_i        :  in std_logic;
      	reset_ni            :  in std_logic;
        
     	enable_o			: out std_logic;
      	cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		addr_o				: out std_logic_vector(7 downto 0);
		tx_data_o			: out std_logic_vector(23 downto 0);
		busy_i				: in std_logic;
		rx_data_i			: in std_logic_vector(15 downto 0)

        );
end;

architecture RTL of dac_max5705 is


type states is ( SETUP, SETUP_WAIT, SETUP_LOOP, 
					  CODE_CHANGE, CODE, CODE_WAIT);
signal state   	: states;

signal rx_data_s	: std_logic_vector(15 downto 0);

begin

	-- drive constant pins
	cpol_o <= '0';
	cpha_o <= '0';
	addr_o <= (others => '0');
	cont_o <= '0';

	-- map signals to ports
	


    -- 
    dac_set : 	process ( reset_ni, clock20mhz_i ) is

	variable count : integer range 0 to 6:= 0;
	
	-- variables for setup lookup table
    type MEM is array(0 to 5) of std_logic_vector(23 downto 0);
    variable dac_setup : MEM;
	
    
    
    begin
	 
	 -- register setup values;
	 dac_setup := ( x"350000", x"250000", x"400000", x"500030", x"600080", x"700000" );
	 
        if ( reset_ni = '0' ) then
            enable_o <= '0';
            count := 0;
				rx_data_s <= (others => '0');
            state <= SETUP;

        elsif Rising_Edge ( clock20mhz_i ) then
        

-- state machine to run the transfer to the SPI module

----------------------------------------------------
-- Send setup data value - loop through each register
----------------------------------------------------

					case state is
						when SETUP =>
							enable_o <= '1';
							tx_data_o <= dac_setup(count);
							if busy_i = '0' then
								state <= SETUP;
							else
								state <= SETUP_WAIT;
							end if;
						when SETUP_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								count := count + 1;
								state <= SETUP_LOOP;
							else
								state <= SETUP_WAIT;
							end if;
					when SETUP_LOOP =>
							enable_o <= '0';
							if count = 6 then
								state <= CODE_CHANGE;
							else
								state <= SETUP;
							end if;


----------------------------------------------------
-- Send CODE value
----------------------------------------------------

						when CODE_CHANGE =>
							if (rx_data_s = rx_data_i) then
								state <= CODE_CHANGE;
							else
								rx_data_s <= rx_data_i;
								state <= CODE;
							end if;
						
						when CODE =>
							enable_o <= '1';
							tx_data_o <= x"80" & rx_data_s;
							if busy_i = '0' then
								state <= CODE;
							else
								state <= CODE_WAIT;
							end if;
						when CODE_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								state <= CODE_CHANGE;
							else
								state <= CODE_WAIT;
							end if;

						-- something went wrong, re-setup dac
		        		when others =>
							state <= SETUP;
					end case;		          
					
        end if;
    end process;


end architecture RTL;