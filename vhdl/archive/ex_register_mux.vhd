-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  PVU Test FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  ex_register_mux.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register mux. Multiplexer for the spi interface to the registers
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ex_register_mux is
	port 	(
	
		clock20m_i						: in std_logic;		
		reset_ni							: in std_logic;		
		
		address_i						: in std_logic_vector(4 downto 0); 		-- register address from spi module
		spi_data_i						: in std_logic_vector(15 downto 0); 	-- data from spi module
		spi_data_o						: out std_logic_vector(15 downto 0);	-- data to spi module
		spi_write_i						: in std_logic;								-- write data to spi module
		spi_load_i						: in std_logic;								-- load spi data from spi module
		
		-- Input registers
		digital_in_i					: in std_logic_vector(6 downto 0); 		-- 
		card_id_i						: in std_logic_vector(4 downto 0); 		-- 

		
		PWM_1_Period_i					: in std_logic_vector(15 downto 0); 	-- 
		PWM_1_Pulse_High_i			: in std_logic_vector(15 downto 0); 	-- 
		PWM_2_Period_i					: in std_logic_vector(15 downto 0); 	-- 
		PWM_2_Pulse_High_i			: in std_logic_vector(15 downto 0); 	-- 
		Tacho_2_Period_i				: in std_logic_vector(15 downto 0); 	-- 
		Tacho_2_Pulse_High_i			: in std_logic_vector(15 downto 0); 	-- 
		Tacho_3_Period_i				: in std_logic_vector(15 downto 0); 	-- 
		Tacho_3_Pulse_High_i			: in std_logic_vector(15 downto 0); 	-- 

		adc_data_i						: in std_logic_vector(255 downto 0); 	-- 
		adc_write_i						: in std_logic;
		
		axle_2_wsp_in_vent_i			: in std_logic;
		axle_2_wsp_in_hold_i			: in std_logic;
		axle_1_wsp_in_vent_i			: in std_logic;
		axle_1_wsp_in_hold_i			: in std_logic;

		crc_error_latched_i			: in std_logic;

		relay_5_nc						: in std_logic;
		relay_5_no						: in std_logic;

		
		-- Outputs
		dac_data_o						: out std_logic_vector(15 downto 0); 	-- Digital outputs - 7 bits used
		relay_drive_o					: out std_logic; 								-- 
		read_relay_drive_o			: out std_logic; 								-- 
		adc_averaging_o				: out std_logic_vector(7 downto 0);		-- 
		digital_9_13_Threshold_o	: out std_logic;						 		-- 
		digital_14_Threshold_o		: out std_logic;						 		-- 
		digital_15_Threshold_o		: out std_logic;						 		-- 
		
		tp5_o								: out std_logic;
		

		digital_9_Enable_o			: out std_logic;
		digital_10_Enable_o			: out std_logic;
		digital_11_Enable_o			: out std_logic;
		digital_12_Enable_o			: out std_logic;
		digital_13_Enable_o			: out std_logic;
		digital_14_Enable_o			: out std_logic;
		digital_15_Enable_o			: out std_logic;
		
		SS_relay_1_drive_o			: out std_logic; 								-- 
		SS_relay_2_drive_o			: out std_logic; 								-- 
		SS_relay_3_drive_o			: out std_logic; 								-- 
		SS_relay_4_drive_o			: out std_logic; 								-- 

		crc_error_latch_reset_o		: out std_logic;
		
		pwm_1_write						: in std_logic;
		pwm_2_write						: in std_logic;
		tacho_2_write					: in std_logic;
		tacho_3_write					: in std_logic
		

		
		
		);
end; 

architecture RTL of ex_register_mux is

	-- signals
	signal spi_data_in_s				: std_logic_vector(15 downto 0);
	signal spi_data_out_s			: std_logic_vector(15 downto 0);
	signal address_read_s			: std_logic_vector(4 downto 0);
	signal address_write_s			: std_logic_vector(4 downto 0);
	signal write_s						: std_logic;
	
	signal register_13_s				: std_logic_vector(15 downto 0);
	signal register_14_s				: std_logic_vector(15 downto 0);
	
	signal pwm_1_period_s			: std_logic_vector(15 downto 0);
	signal pwm_1_pulse_high_s		: std_logic_vector(15 downto 0);
	signal pwm_2_period_s			: std_logic_vector(15 downto 0);
	signal pwm_2_pulse_high_s		: std_logic_vector(15 downto 0);

	signal tacho_2_period_s			: std_logic_vector(15 downto 0);
	signal tacho_2_pulse_high_s	: std_logic_vector(15 downto 0);
	signal tacho_3_period_s			: std_logic_vector(15 downto 0);
	signal tacho_3_pulse_high_s	: std_logic_vector(15 downto 0);

	signal adc_1_1_s					: std_logic_vector(15 downto 0);
	signal adc_1_2_s					: std_logic_vector(15 downto 0);
	signal adc_1_3_s					: std_logic_vector(15 downto 0);

	signal adc_2_1_s					: std_logic_vector(15 downto 0);
	signal adc_2_2_s					: std_logic_vector(15 downto 0);
	signal adc_2_3_s					: std_logic_vector(15 downto 0);
	
	signal adc_3_1_s					: std_logic_vector(15 downto 0);
	signal adc_3_2_s					: std_logic_vector(15 downto 0);
	signal adc_3_3_s					: std_logic_vector(15 downto 0);
	signal adc_3_4_s					: std_logic_vector(15 downto 0);

	signal adc_4_1_s					: std_logic_vector(15 downto 0);
	signal adc_4_2_s					: std_logic_vector(15 downto 0);
	signal adc_4_3_s					: std_logic_vector(15 downto 0);
	
	signal register_23_s				: std_logic_vector(15 downto 0);
	signal register_24_s				: std_logic_vector(15 downto 0);
	
	
	begin
		
	--map registers to ports
	spi_data_o <= spi_data_out_s;

	
	
	SS_relay_1_drive_o			<= register_23_s(0);
	SS_relay_2_drive_o			<= register_23_s(1);
	SS_relay_3_drive_o			<= register_23_s(2);
	SS_relay_4_drive_o			<= register_23_s(3);

	read_relay_drive_o			<= register_23_s(4);
	relay_drive_o					<= register_23_s(5);
	
	digital_9_Enable_o			<= register_23_s(6);
	digital_10_Enable_o			<= register_23_s(7);
	digital_11_Enable_o			<= register_23_s(8);
	digital_12_Enable_o			<= register_23_s(9);
	digital_13_Enable_o			<= register_23_s(10);
	digital_14_Enable_o			<= register_23_s(11);
	digital_15_Enable_o			<= register_23_s(12);
	
	digital_9_13_Threshold_o 	<= register_24_s(0);
	digital_14_Threshold_o		<= register_24_s(1);
	digital_15_Threshold_o		<= register_24_s(2);
	crc_error_latch_reset_o	 	<= register_24_s(3);
	tp5_o							 	<= register_24_s(4);
	
	register_13_s(0)				<= axle_2_wsp_in_vent_i;
	register_13_s(1)				<= axle_2_wsp_in_hold_i;
	register_13_s(2)				<= axle_1_wsp_in_vent_i;
	register_13_s(3)				<= axle_1_wsp_in_hold_i;
	register_13_s(4) 				<= digital_in_i(5) and register_24_s(5);
	register_13_s(5) 				<= digital_in_i(6) and register_24_s(6);
	register_13_s(6) 				<= crc_error_latched_i;
	register_13_s(7)				<= relay_5_nc;
	register_13_s(8)				<= relay_5_no;
	register_13_s(13 downto 9) <= digital_in_i(4 downto 0);
	register_13_s(15 downto 14)<= "00";

	register_14_s(8 downto 0) 	<= "000000000";
	register_14_s(13 downto 9) <= card_id_i(4 downto 0);
	register_14_s(15 downto 14)<= "00";
	
	
	-- receive data from spi module and send to registers
	mux_write : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			address_write_s	<= (others => '0');
			spi_data_in_s		<= (others => '0');
			dac_data_o			<= (others => '0');
			register_23_s		<= (others => '0');
			register_24_s		<= (others => '0');
			adc_averaging_o	<= (others => '0');
			write_s 				<= '0';
			

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			-- handle write from spi module
			if ( spi_write_i = '1' ) then
				spi_data_in_s <= spi_data_i;
				address_write_s <= address_i;
				write_s 		<= '1';
			else
				spi_data_in_s <= spi_data_in_s;
				write_s 		<= '0';
			end if;
			
			-- handle write registers
			if ( write_s = '1' ) then
			
				case address_write_s is
						
					when "10111" => register_23_s 	<= spi_data_in_s;
					when "11000" => register_24_s 	<= spi_data_in_s;
					when "11001" => dac_data_o 		<= spi_data_in_s;
					when "11010" => adc_averaging_o	<= spi_data_in_s(7 downto 0);

					when others => null;
				end case;
			
			end if;
		
			
	 	end if;
	end process;

	
	
	-- transmit data to spi module from registers
	mux_read : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			address_read_s <= (others => '0');
			spi_data_out_s	<= (others => '0');
			

			pwm_1_period_s		<= (others => '0');
			pwm_1_pulse_high_s	<= (others => '0');
			pwm_2_period_s		<= (others => '0');
			pwm_2_pulse_high_s	<= (others => '0');

			tacho_2_period_s		<= (others => '0');
			tacho_2_pulse_high_s	<= (others => '0');
			tacho_3_period_s		<= (others => '0');
			tacho_3_pulse_high_s	<= (others => '0');

			adc_1_1_s		<= (others => '0');
			adc_1_2_s		<= (others => '0');
			adc_1_3_s		<= (others => '0');

			adc_2_1_s		<= (others => '0');
			adc_2_2_s		<= (others => '0');
			adc_2_3_s		<= (others => '0');

			adc_3_1_s		<= (others => '0');
			adc_3_2_s		<= (others => '0');
			adc_3_3_s		<= (others => '0');
			adc_3_4_s		<= (others => '0');

			adc_4_1_s		<= (others => '0');
			adc_4_2_s		<= (others => '0');
			adc_4_3_s		<= (others => '0');


			
	
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 


			address_read_s <= address_i;
		
			
			-- handle write registers
			if ( spi_load_i = '1' ) then
			
				case address_read_s is

						
					when "00000" => spi_data_out_s <= adc_1_1_s;
					when "00001" => spi_data_out_s <= adc_1_2_s;
					when "00010" => spi_data_out_s <= adc_1_3_s;
					
					when "00011" => spi_data_out_s <= adc_2_1_s;
					when "00100" => spi_data_out_s <= adc_2_2_s;
					when "00101" => spi_data_out_s <= adc_2_3_s;
					
					when "00110" => spi_data_out_s <= adc_3_1_s;
					when "00111" => spi_data_out_s <= adc_3_2_s;
					when "01000" => spi_data_out_s <= adc_3_3_s;
					when "01001" => spi_data_out_s <= adc_3_4_s;

					when "01010" => spi_data_out_s <= adc_4_1_s;
					when "01011" => spi_data_out_s <= adc_4_2_s;
					when "01100" => spi_data_out_s <= adc_4_3_s;

					when "01101" => spi_data_out_s <= register_13_s;
					when "01110" => spi_data_out_s <= register_14_s;
					when "01111" => spi_data_out_s <= pwm_1_period_s;
					when "10000" => spi_data_out_s <= pwm_1_pulse_high_s;
					when "10001" => spi_data_out_s <= pwm_2_period_s;
					when "10010" => spi_data_out_s <= pwm_2_pulse_high_s;
					
					when "10011" => spi_data_out_s <= tacho_2_period_s;
					when "10100" => spi_data_out_s <= tacho_2_pulse_high_s;
					when "10101" => spi_data_out_s <= tacho_3_period_s;
					when "10110" => spi_data_out_s <= tacho_3_pulse_high_s;
					
					
					when others => null;
				end case;
			
			
			end if;
			
			
			
			if( adc_write_i = '1' and spi_load_i /= '1') then
				adc_1_1_s <= adc_data_i(255 downto 240);
				adc_1_2_s <= adc_data_i(239 downto 224);
				adc_1_3_s <= adc_data_i(223 downto 208);
				
				adc_2_1_s <= adc_data_i(191 downto 176);
				adc_2_2_s <= adc_data_i(175 downto 160);
				adc_2_3_s <= adc_data_i(159 downto 144);
				
				adc_3_1_s <= adc_data_i(127 downto 112);
				adc_3_2_s <= adc_data_i(111 downto 96);
				adc_3_3_s <= adc_data_i(95 downto 80);
				adc_3_4_s <= adc_data_i(79 downto 64);

				adc_4_1_s <= adc_data_i(63 downto 48);
				adc_4_2_s <= adc_data_i(47 downto 32);
				adc_4_3_s <= adc_data_i(31 downto 16);
			else
				adc_1_1_s <= adc_1_1_s;
				adc_1_2_s <= adc_1_2_s;
				adc_1_3_s <= adc_1_3_s;
				
				adc_2_1_s <= adc_2_1_s;
				adc_2_2_s <= adc_2_2_s;
				adc_2_3_s <= adc_2_3_s;
				
				adc_3_1_s <= adc_3_1_s;
				adc_3_2_s <= adc_3_2_s;
				adc_3_3_s <= adc_3_3_s;
				adc_3_4_s <= adc_3_4_s;

				adc_4_1_s <= adc_4_1_s;
				adc_4_2_s <= adc_4_2_s;
				adc_4_3_s <= adc_4_3_s;
			end if;

			-- handle pwm 1 input
			if(pwm_1_write = '1' and 	spi_load_i /= '1') then
				if(register_24_s(5) = '0') then
					pwm_1_period_s <= pwm_1_period_i;
					pwm_1_pulse_high_s <= pwm_1_pulse_high_i;
				else
					pwm_1_period_s <= (others => '0');
					pwm_1_pulse_high_s <= (others => '0');
				end if;
			else		
				pwm_1_period_s  <= pwm_1_period_s;
				pwm_1_pulse_high_s <= pwm_1_pulse_high_s;
			end if;

			
			-- handle pwm 2 input
			if(pwm_2_write = '1' and 	spi_load_i /= '1') then
				if(register_24_s(6) = '0') then
					pwm_2_period_s <= pwm_2_period_i;
					pwm_2_pulse_high_s <= pwm_2_pulse_high_i;
				else
					pwm_2_period_s <= (others => '0');
					pwm_2_pulse_high_s <= (others => '0');
				end if;
			else		
				pwm_2_period_s  <= pwm_2_period_s;
				pwm_2_pulse_high_s <= pwm_2_pulse_high_s;
			end if;
			
			-- handle tacho 2 input
			if(tacho_2_write = '1' and 	spi_load_i /= '1') then
				tacho_2_period_s <= tacho_2_period_i;
				tacho_2_pulse_high_s <= tacho_2_pulse_high_i;
			else		
				tacho_2_period_s  <= tacho_2_period_s;
				tacho_2_pulse_high_s <= tacho_2_pulse_high_s;
			end if;			


			-- handle tacho 3 input
			if(tacho_3_write = '1' and 	spi_load_i /= '1') then
				tacho_3_period_s <= tacho_3_period_i;
				tacho_3_pulse_high_s <= tacho_3_pulse_high_i;
			else		
				tacho_3_period_s  <= tacho_3_period_s;
				tacho_3_pulse_high_s <= tacho_3_pulse_high_s;
			end if;			

			
	 	end if;
	end process;
	
	
end architecture RTL;