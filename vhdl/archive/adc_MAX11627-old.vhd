-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_MAX11627.vhd
--                   Author  :  Mark Bonning 
--                     Date  :  12/11/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generic spi slave interface. To be used on all FPGAs within the project
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. Changed to 4 channel device.
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity adc_MAX11627 is
	port 	(
		adc_dout_i					:  in std_logic;
		adc_eoc_i					:  in std_logic;
		adc_din_o					: out std_logic;
		adc_cs_o						: out std_logic;
		clock10mhz_adc_i    		:  in std_logic; 
		reset_ni			    		:  in std_logic;
		ch1_o             		: out std_logic_vector(15 downto 0);
		ch2_o             		: out std_logic_vector(15 downto 0);
		ch3_o              		: out std_logic_vector(15 downto 0);
		ch4_o              		: out std_logic_vector(15 downto 0);
		adc_clk_out_o				: out std_logic
		);
end;

architecture RTL of adc_MAX11627 is

	signal adc_register_channel_s : std_logic;
	
	signal ch1_s	   	   : unsigned(15 downto 0);
	signal ch2_s  	      : unsigned(15 downto 0);
	signal ch3_s  	      : unsigned(15 downto 0);
	signal ch4_s  	      : unsigned(15 downto 0);
	
	signal tx_buf_s  			: std_logic_vector(7 downto 0); 
	signal adc_clk_send_s  	: std_logic;
	signal adc_cs_s		: std_logic;
	signal rx_buf_s			: unsigned(127 downto 0);

	type state_cm_wr is ( READY, ADC_START, ADC_DATA_STOP, ADC_DATA_RECEIVECH0,
	ADC_DATA_CH0, ADC_DATA_FINISH, ADC_DATA_WAIT, ADC_DATA_WAIT1, ADC_DATA_SEND,
	ADC_WAIT2, ADC_DATA_WAIT_SETUP, READY_SETUP, ADC_START_SETUP, 
	ADC_DATA_SEND_SETUP, ADC_DATA_STOP_SETUP, ADC_WAIT);
	
	
	signal transmit_s			: std_logic;
	signal transmit_setup_s	: std_logic;
	signal set_sync_sm_s   	: state_cm_wr;

	attribute keep          : boolean;

-- need to set the conversion and setup registers for correct operation of the ADC.
constant SETUP_REGISTER         : std_logic_vector( 7 downto 0) := "01101000";
-- data set to ADC to request which channels
constant CONVERSION_REGISTER    : std_logic_vector( 7 downto 0) := "10011000";


begin



	-- latch data to the correct register depending 
	address_decode_data : process (clock10mhz_adc_i, reset_ni) is
	begin
		if reset_ni = '0' then
			ch1_s 			<= (others => '0');
			ch2_s 			<= (others => '0');
			ch3_s 			<= (others => '0');
			ch4_s 			<= (others => '0');
			
			
		elsif rising_edge ( clock10mhz_adc_i ) then
			if adc_register_channel_s = '1' then	   
				ch1_s 		<= rx_buf_s(15 downto 0);
				ch2_s 		<= rx_buf_s(31 downto 16);
				ch3_s 		<= rx_buf_s(47 downto 32);
				ch4_s 		<= rx_buf_s(63 downto 48);

				end if;
		end if;
	end process;

	ch1_o <= std_logic_vector(ch1_s);
	ch2_o <= std_logic_vector(ch2_s);
	ch3_o <= std_logic_vector(ch3_s);
	ch4_o <= std_logic_vector(ch4_s);
	
	
-- state machine to control the ADC
	process (reset_ni, clock10mhz_adc_i) is
		variable count : integer range 0 to 130:= 0;
	begin
		if reset_ni = '0' then
			set_sync_sm_s 		<= READY_SETUP;
			adc_cs_s 			<= '1';
			transmit_s 			<= '0';
			transmit_setup_s 	<= '0';
			adc_clk_send_s		<= '0';
			count 				:=  0; 
			adc_register_channel_s <= '0';
			rx_buf_s 					<= (others => '0');
			
			
		elsif rising_Edge ( clock10mhz_adc_i ) then
			case set_sync_sm_s is
				when READY_SETUP =>
					count := count + 1;
					transmit_setup_s 		<= '1';
					
					-- wait for a time to allow the system to settle
					if count = 99 then  
					--if count = 40 then -- simulation only
						set_sync_sm_s      		<= ADC_START_SETUP;
					else
						set_sync_sm_s      		<= READY_SETUP;
					end if;
				-- set the cs low to start transfer of set up data to ADC, this is only done once
				when ADC_START_SETUP =>
					adc_cs_s 		<= '0';
					count 			:=0; 
					transmit_setup_s <= '0';
					transmit_s 		<= '1';
					set_sync_sm_s      			<= ADC_DATA_SEND_SETUP;
				-- send the clock to the ADC
				-- requests data from the adc by sending 8 bits of data
				when ADC_DATA_SEND_SETUP =>
				adc_clk_send_s <= '1';
				count := count + 1;
					if count = 8 then
						set_sync_sm_s      		<= ADC_DATA_STOP_SETUP;
					else
						set_sync_sm_s      		<= ADC_DATA_SEND_SETUP;
					end if;
				when ADC_DATA_STOP_SETUP =>
					count := 0; 
					adc_clk_send_s <= '0';
					transmit_s 		<= '0';
					adc_cs_s 			<= '1';
					set_sync_sm_s      		<= ADC_DATA_WAIT_SETUP;
					-- wait for 20 clock cycles after sending the setup register
				when ADC_DATA_WAIT_SETUP => 
				count := count + 1;
					if count = 20 then -- 
						set_sync_sm_s      		<= READY;
					else
						set_sync_sm_s      		<= ADC_DATA_WAIT_SETUP;
					end if;	
				when READY =>
					count := count + 1;
					--if count = 40 then -- simulation only
					if count = 99 then -- wait for 99 clock cycles before sending the data request
						set_sync_sm_s      		<= ADC_START;
					else
						set_sync_sm_s      		<= READY;
					end if;
				when ADC_START =>
					adc_cs_s 			<= '0';
					count :=0; 
					transmit_s 		<= '1';
					set_sync_sm_s      			<= ADC_DATA_SEND;
					
				when ADC_DATA_SEND =>
--				adc_clk_send_s <= '1';
				count := count + 1;
				
					if count = 8 then
						set_sync_sm_s      		<= ADC_DATA_STOP;
						adc_clk_send_s <= '0';
					else
						set_sync_sm_s      		<= ADC_DATA_SEND;
						adc_clk_send_s <= '1';
						
					end if;
				when ADC_DATA_STOP =>
					count := 0; 
					transmit_s 		<= '0';
					adc_clk_send_s <= '0';
					adc_cs_s 			<= '1';
					if  = '0' then
						set_sync_sm_s      		<= ADC_DATA_WAIT;
					else
						set_sync_sm_s      		<= ADC_DATA_STOP;
					end if;
				when ADC_DATA_WAIT =>
					set_sync_sm_s      			<= ADC_DATA_WAIT1;
				when ADC_DATA_WAIT1 =>
				adc_cs_s 			<= '0';
					set_sync_sm_s      			<= ADC_DATA_RECEIVECH0;
				when ADC_DATA_RECEIVECH0 =>
					
					adc_clk_send_s <= '1';
					count :=0; 
					set_sync_sm_s      			<= ADC_DATA_CH0;
				when ADC_DATA_CH0 =>
				count := count + 1;
				rx_buf_s 			<= rx_buf_s(126 downto 0) & adc_dout_i; 
				
					if count = 127 then
						set_sync_sm_s      		<= ADC_DATA_FINISH;
						
					else
						set_sync_sm_s      		<= ADC_DATA_CH0;
					end if;
					
				
				when ADC_DATA_FINISH =>
					adc_cs_s 			<= '1';
					adc_clk_send_s <= '0';
					adc_register_channel_s <= '1';
					count :=0; 
					set_sync_sm_s      		<= ADC_WAIT;
				when ADC_WAIT =>
					adc_cs_s 			<= '1';
					set_sync_sm_s      		<= ADC_WAIT2;
				when ADC_WAIT2 =>
					adc_register_channel_s <= '0';
					set_sync_sm_s      		<= READY;	

					

				when others =>
			end case;
		end if;
	end process;
	
	-- transmit data to spi bus
	transmit_data : process (reset_ni, clock10mhz_adc_i)
	begin
		--transmit registers
		if reset_ni = '0' then
			tx_buf_s <= SETUP_REGISTER;
			adc_din_o <= '0';
		elsif falling_edge(clock10mhz_adc_i) then		
			if transmit_s = '1' then	
				tx_buf_s <= tx_buf_s(6 downto 0) & '0';  --shift through tx data
				adc_din_o <= tx_buf_s(7);  
			elsif transmit_setup_s = '1' then -- send setup register information
				tx_buf_s <= SETUP_REGISTER;
			else
				tx_buf_s<= CONVERSION_REGISTER; -- send which conversion register 
				adc_din_o <= '0';	
			end if;
		end if;
	end process;
	
	
	-- sets the clk out to adc
	process (reset_ni, adc_clk_send_s, clock10mhz_adc_i) is
	begin
		if reset_ni = '0' then
			adc_clk_out_o 			<= '0';

		elsif adc_clk_send_s = '1' then
			adc_clk_out_o <= clock10mhz_adc_i; 
		else
			adc_clk_out_o 			<= '0';
		end if;
	end process;
adc_cs_o <= adc_cs_s;
end architecture RTL;