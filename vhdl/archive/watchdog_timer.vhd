-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  watchdog_timer.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/01/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Watchdog timer - maintains a pulse to the watchdog as long as an adc 
-- End Of Conversion (EOC) has taken place within 200ms
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity watchdog_timer is
    port 	(

        clock20mhz_i           :  in std_logic;
        reset_ni               :  in std_logic;
        eoc_i				: in std_logic;
		watchdog_kick_o  		: out std_logic
        );
end;

architecture RTL of watchdog_timer is

	signal clock1ms_s		: std_logic;
	signal clock1ms_counter_s	: unsigned(14 downto 0);
	constant clock1ms_limit		: unsigned(14 downto 0) := "010011100010000";
	

	signal   counter_watchdog_s 		  : unsigned(7 downto 0) ;
	constant 	 watchdog_limit_s		  : unsigned(7 downto 0) := "00101000";	-- 80ms
	signal watchdog_kick_s : std_logic;

	signal   counter_timeout_s 		  : unsigned(7 downto 0) ;
	constant 	 timeout_limit_s		  : unsigned(7 downto 0) := "11001000";	-- 200ms

	signal eoc_current_s			: std_logic;
	
begin

	-- map signals to ports
	   watchdog_kick_o <= watchdog_kick_s;

    --counter to divide the 20mhz down to 1khz - make the timer more manageable
    clock_div : 	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            clock1ms_counter_s <= ( others => '0' );
				clock1ms_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then
				if clock1ms_counter_s = clock1ms_limit then
					clock1ms_counter_s <= ( others => '0' ) ;
					clock1ms_s <= not (clock1ms_s);
				else
					clock1ms_counter_s <= clock1ms_counter_s + 1;
				end if;
        end if;
    end process;



    --counter to set the watchdog refresh signal
    watchdog : 	process ( reset_ni, clock1ms_s, eoc_i ) is
    begin
        if ( reset_ni = '0' ) then
            counter_watchdog_s <= ( others => '0' );
				watchdog_kick_s <= '0';
            counter_timeout_s <= ( others => '0' );
            eoc_current_s <= eoc_i;
        elsif Rising_Edge ( clock1ms_s ) then
        
        		--reset the count at the end of a cycle
				if counter_watchdog_s = watchdog_limit_s then
					counter_watchdog_s <= ( others => '0' ) ;
					
					-- invert watchdog if no timeout
					if(counter_timeout_s < timeout_limit_s) then
						-- invert the watchdog kick
						watchdog_kick_s <= not (watchdog_kick_s);
					end if;
				else
					counter_watchdog_s <= counter_watchdog_s + 1;
				end if;

        			--hold the count if limit reached
				if(counter_timeout_s = timeout_limit_s) then
					counter_timeout_s <= timeout_limit_s;
        			--reset the count if eoc level changed
				elsif (eoc_current_s /= eoc_i) then
					counter_timeout_s <= ( others => '0' ) ;
					eoc_current_s <= eoc_i;
				else
					counter_timeout_s <= counter_timeout_s + 1;
				end if;


        end if;
    end process;



end architecture RTL;