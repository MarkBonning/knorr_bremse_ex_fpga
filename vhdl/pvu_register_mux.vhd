-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pvu_register_mux.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  21/11/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register mux from PVU to EX
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pvu_register_mux is
	port 	(
	
		spi_clk_i      				: in std_logic;  								-- spi clk from master
		spi_cs_ni        				: in std_logic;  								-- active low slave select
		spi_mosi_i         			: in std_logic;  								-- master out, slave in
		spi_miso_o         			: out std_logic;  								-- master in, slave out

		-- read regiters
		digital_in_1_filtered_i		: in std_logic;
		digital_in_2_filtered_i		: in std_logic;
		digital_in_3_filtered_i		: in std_logic;
		digital_in_4_filtered_i		: in std_logic;
		digital_in_5_filtered_i		: in std_logic;
		digital_in_6_filtered_i		: in std_logic;
		digital_in_7_filtered_i		: in std_logic;
		
		reg_01_i							: in std_logic_vector(15 downto 0); 	
		reg_02_i							: in std_logic_vector(15 downto 0); 	
		reg_03_i							: in std_logic_vector(15 downto 0); 	
		reg_04_i							: in std_logic_vector(15 downto 0) 	
		
		);
end; 

architecture RTL of pvu_register_mux is

	-- signals

	
	signal tx_data_s  				: std_logic_vector(0 to 95);  					-- spi transmit buffer (address copy, data and crc)
	signal tx_bit_count_s 			: unsigned(7 downto 0);
	
	signal reg_00_s					: std_logic_vector(15 downto 0);
	 
begin
		
	--map registers to ports

	reg_00_s <= "000000000" & digital_in_7_filtered_i & digital_in_6_filtered_i & digital_in_5_filtered_i & 
					digital_in_4_filtered_i & digital_in_3_filtered_i & digital_in_2_filtered_i & digital_in_1_filtered_i;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPI TX
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	spi_tx : process (spi_cs_ni, spi_clk_i)
	
	
	begin
		-- reset when chip select is hihj
		if spi_cs_ni = '1' then

			tx_bit_count_s <= x"01";
			
			tx_data_s <= reg_00_s & reg_01_i & reg_02_i & reg_03_i & reg_04_i & x"0000";
			
			spi_miso_o <= '0';

		-- SPI data changing on falling edge
		elsif falling_edge (spi_clk_i) then

			-- load miso
			spi_miso_o <= tx_data_s(to_integer(tx_bit_count_s));  
			
			-- calculate the xsum and load to the tx buffer
			if tx_bit_count_s = 1 then
				tx_data_s(80 to 95) <= std_logic_vector(unsigned(reg_00_s) + unsigned(reg_01_i) + unsigned(reg_02_i) + unsigned(reg_03_i) + unsigned(reg_04_i));
			end if;
			
			-- increase the count if less that 95, stops buffer corruption if transfer goes on to many bits
			if tx_bit_count_s < 95 then
				tx_bit_count_s <= unsigned(tx_bit_count_s) + 1;
			end if;

		end if;
	end process;	

	
	
end architecture RTL;