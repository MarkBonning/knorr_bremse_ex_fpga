-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  card_id.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/11/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Monitors the card ID, if correct enables all outputs, 
-- if incorrect, tri-states all outputs
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity card_id is
    port (
	 
		-- card id inputs from resitor selection matrix
		card_id_4_i			: in std_logic;
		card_id_3_i			: in std_logic;
		card_id_2_i			: in std_logic;
		card_id_1_i			: in std_logic;
		card_id_0_i			: in std_logic;

		-- 1 = enable outputs, 0 = tri state outputs
		output_enable_o	: out std_logic
		
    );
end entity card_id;

architecture imp_card_id of card_id is

	

begin


-- logic

	-- enable the output if the card ID is 0x1b;
	output_enable_o <= '1' when 	card_id_4_i = '0' and card_id_3_i = '0' and 
											card_id_2_i = '0' and card_id_1_i = '0' and 
											card_id_0_i = '1' else '0';
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_card_id;

